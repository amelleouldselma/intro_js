export function exo() {
	console.log('Exercice 1');
}

// Exercice 1.1
/*Créer une variable et l'initialiser à 0. Tant que cette variable n'atteint pas 10 :

    l'afficher
    incrémenter de 1*/ 

export function n1(){
        var index = 0;
        while (index < 10){
            index++;
            console.log ('Exercice 1 le Passage dans la boucle numéro ' + index);
    }
}

// Exercice 1.2
/* Créer deux variables. Initialiser la première à 0 et la deuxième avec un nombre 
   compris en 1 et 100. Tant que la première variable n'est pas supérieur à 20 :
        multiplier la première variable avec la deuxième
        afficher le résultat
        incrémenter la première variable*/

export function n2 () {
        var a = 0;
        var b = 33;

        while ( a < 20){
            console.log ("Le résultat de l'exercice 2 est" + (a*b));
            a ++;
        }
}

// Exercice 1.3
/* Créer deux variables. Initialiser la première à 100 et la deuxième avec un nombre compris en 1 et 100. Tant que la première variable n'est pas inférieur ou égale à 10 :

    multiplier la première variable avec la deuxième
    afficher le résultat
    décrémenter la première variable */

export function n3 (){
        var c = 100;
        var d = 55;

        while ( c < 20){
            console.log ("Le résultat de l'exercice 3 est" + (c*d));
            c --;
        }
}

// Exercice 1.4
/* Créer une variable et l'initialiser à 1. Tant que cette variable n'atteint pas 10 :

    l'afficher
    l'incrementer de la moitié de sa valeur*/

export function n4 () {


    while( i < 10){    
        
        var i = 1;        
        i = i + (i/2);
        console.log ("Le résultat de l'exercice 4 est" + (i));
    }
}


//Exercice 5 En allant de 1 à 15 avec un pas de 1, afficher le message On y arrive presque...

export function n5 () {

    for (var i = 1; i < 15; i++) {
        console.log ("Exo 5 On y arrive presque... " + (i));
    }

}

//Exercice 6 En allant de 20 à 0 avec un pas de 1, afficher le message C'est presque bon...

export function n6 () {

    for (var i = 20; i >= 0; i--) {
        console.log (" Exo 6 C'est presque bon... " + (i));
    }

}

//Exercice 7 En allant de 1 à 100 avec un pas de 15, afficher le message On tient le bon bout...

export function n7 () {

    for (var i = 1; i < 100; i+=15) {
        console.log ("exo 7 On tient le bon bout... " + (i));
    }

}


//Exercice 8 En allant de 200 à 0 avec un pas de 12, afficher le message Enfin ! ! ! 

export function n8 () {

    for (var i = 200; i >= 0; i-=12) {
        console.log ("exo 8 Enfin ! ! !  " + (i));
    }

}